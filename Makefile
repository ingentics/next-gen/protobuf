.PHONY: tidy proto

tidy:
	go mod tidy

proto:
	cd proto && protoc --go_out="." "--go_opt=paths=source_relative" *.proto
