# Next Gen Protobuf

Welcome to the Next-Gen project's shared Protobuf repository! 
This repository contains the Protobuf files that define the shared data structures used in the Next-Gen project.
These files are used to generate code in various languages that can be used to serialize and deserialize the Next-Gen project's data and perform RPC calls to the implemented service.

The repository contains the following Protobuf files:

1. `pager.proto` the structure for search pagination.
1. `arangodb.proto` the options to generate an ArangoDB entity.
1. `gorm.proto` the options to generate an Gorm entity.
1. `generate.proto` extra options for shared entity generation behavior.

## Usage

Using [protodeb](https://github.com/stormcat24/protodep) tool:

```toml
proto_outdir = "./proto/deps"

[[dependencies]]
  target = "gitlab.com/ingentics/next-gen/protobuf/proto"
  subgroup = "next-gen"
  branch = "main"
  path = "next-gen"

```