module gitlab.com/ingentics/next-gen/protobuf.git

go 1.19

require google.golang.org/protobuf v1.28.1
